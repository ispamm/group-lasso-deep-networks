Group sparse regularization for deep neural networks (Python)

Description
-------
The code implements the group sparse formulation described in:
	https://arxiv.org/abs/1607.00485
	
It is based on a group Lasso penalty that imposes sparsity on a
neuron level, in order to promote a low number of neurons at every
layer. All the code is contained in the 'run_simulation.py' script.

Dependencies
-------
The following libraries are used in the code:
	* Python v3.5 (should work on 2.7, but several print statements must be changed).
        * Numpy v1.11.0.
	* Theano v0.7.0.
        * Lasagne v0.2.dev1.

Licensing
-------
The code is distributed under BSD-2 license. Please see the file called LICENSE.

References
-------
[1] S. Scardapane, D. Comminiello, A. Hussain and A. Uncini, 2016, "Group Sparse
	Regularization for Deep Neural Networks", https://arxiv.org/abs/1607.00485.
[2] Simon, N., Friedman, J., Hastie, T. and Tibshirani, R., 2013. "A sparse-group 
	lasso". Journal of Computational and Graphical Statistics, 22(2), pp. 231-245.
	
Contacts
-------
   o If you have any request, bug report, or inquiry, you can contact
     the author at simone [dot] scardapane [at] uniroma1 [dot] it.
   o Additional contact information can also be found on the website of
     the author:
	      http://ispac.ing.uniroma1.it/scardapane/